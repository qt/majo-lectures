{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to 马约拉纳 fermions\n",
    "\n",
    "*Anton Akhmerov, Delft University of Technology*  \n",
    "\n",
    "*2018 Tsinghua Quantum Computing Summer School, 20 May 2018*\n",
    "    \n",
    "Based on the online course \"Topology in condensed matter\", https://topocondmat.org  \n",
    "Code uses the Kwant package for quantum transport simulations, https://kwant-project.org  \n",
    "Source repository: http://antonakhmerov.org/misc/majoranas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lecture goals\n",
    "\n",
    "* Lecture 1: Majorana basics\n",
    "    - Introduce Majorana states\n",
    "    - Explain why Majorana are protected ($\\Rightarrow$ relevant for quantum computation)\n",
    "    - Create Majorana in a toy model\n",
    "* Lecture 2: Majorana in real systems\n",
    "    - Create Majorana in a realistic model\n",
    "* Lecture 3: detecting Majorana\n",
    "    - Majorana supercurrent\n",
    "    - Majorana conductance\n",
    "\n",
    "Later today:\n",
    "* Fabian Hassler will teach more about quantum computing with Majoranas\n",
    "* Hao Zhang will tell about experiments for detecting Majoranas\n",
    "\n",
    "### Lecture design:\n",
    "\n",
    "* Not systematic\n",
    "* Not historical\n",
    "* Skipping the difficult parts\n",
    "* Hands-on"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lecture 1: Majorana basics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Qubits, fermions, and Majoranas\n",
    "\n",
    "A qubit is a 2-level system: \n",
    "- 2 states, $|0\\rangle$ and $|1\\rangle$\n",
    "- qubit operators $\\sigma^x$, $\\sigma^y$, $\\sigma^z$\n",
    "\n",
    "A fermion is also a 2-level system:\n",
    "- 2 states, $|0\\rangle$ (empty) and $|1\\rangle$ (occupied)\n",
    "- fermion operators $c^\\dagger$, $c$, $n = c^\\dagger c$\n",
    "\n",
    "However *fermion parity* is conserved: $c + c^\\dagger$ is Hermitian but never appears in the Hamiltonian.\n",
    "\n",
    "A trapped fermion will never \"decohere\" to an empty state without any error correction.\n",
    "\n",
    "Unfortunately this isn't useful because:\n",
    "* Dephasing of fermions is super fast\n",
    "* It's impossible to make a superposition $\\tfrac{1}{\\sqrt{2}}(|0\\rangle+|1\\rangle)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Idea: split a fermion into 2 parts\n",
    "\n",
    "Idea what if we split a fermion into:\n",
    "\n",
    "$$ c^\\dagger = \\tfrac{1}{2}(\\gamma_1+i\\gamma_2),\\;\\; c = \\tfrac{1}{2}(\\gamma_1-i\\gamma_2).$$\n",
    "\n",
    "Here $\\gamma_1 = (c^\\dagger+c)$ and $\\gamma_2=(c^\\dagger-c)/i$ are **Majorana operators**.\n",
    "\n",
    "Because $\\gamma_1=\\gamma_1^\\dagger$ and $\\gamma_2=\\gamma_2^\\dagger$ a single Majorana mode is neither 'empty' nor 'filled'!\n",
    "\n",
    "### Commutation\n",
    "\n",
    "Majoranas are similar to fermions:\n",
    "\n",
    "$$\\gamma_1\\gamma_2 + \\gamma_2\\gamma_1 = 0\\;,\\;\\gamma_1^2=1\\;,\\;\\gamma_2^2=1\\;.$$\n",
    "\n",
    "* Majorana modes also anticommute.\n",
    "* Using Majoranas instead of fermions is like changing a complex number $\\rightarrow$ two real numbers.\n",
    "\n",
    "### Protection\n",
    "\n",
    "If the Hilbert space contains 1 Majorana, $H=0$: **Isolated Majoranas are protected!**\n",
    "\n",
    "The Hamiltonian must also couple at least two Majoranas: $H=2i\\epsilon \\gamma_1\\gamma_2$.\n",
    "\n",
    "Can we use this for protecting quantum information? No: we only have fermions to work with :-(\n",
    "\n",
    "However turns out there is a trick!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Unpaired Majorana modes in a model of dominoes\n",
    "\n",
    "Let us consider a chain of $N$ fermions $c^\\dagger_n$ (two Majorana modes $\\gamma_{2n-1}$ and $\\gamma_{2n}$):\n",
    "\n",
    "![](figures/majorana_dominoes.svg)\n",
    "\n",
    "What happens if we pair the fermions? The Hamiltonian becomes\n",
    "\n",
    "$$H=(i/2)\\,\\mu\\, \\sum_{n=1}^{N} \\gamma_{2n-1}\\gamma_{2n}.$$\n",
    "\n",
    "This is how the pairing looks:\n",
    "\n",
    "![](figures/trivial_dominoes.svg)\n",
    "\n",
    "All the excitations have $E=\\pm|\\mu|/2$, the chain has a gapped bulk and no zero energy edge states. (Boring)\n",
    "\n",
    "We want to isolate one Majorana $\\Rightarrow$ pair up the Majoranas from *adjacent* sites:\n",
    "\n",
    "![](figures/topological_dominoes.svg)\n",
    "\n",
    "Now the Hamiltonian is:\n",
    "\n",
    "$$H=it \\sum_{n=1}^{N-1} \\gamma_{2n}\\gamma_{2n+1}.$$\n",
    "\n",
    "Two end Majorana modes $\\gamma_1$ and $\\gamma_{2N}$ do not appear in $H$ at all and have $E=0$.\n",
    "\n",
    "All the other states have $E=\\pm |t|$ $\\Rightarrow$ we have a one-dimensional system with a gapped bulk and zero energy states at the edges."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Kitaev chain\n",
    "\n",
    "Rewriting this model in terms of fermions: $\\gamma_{2n-1} \\rightarrow (c_n^\\dagger+c_n)$ and $\\gamma_{2n} \\rightarrow -i(c_n^\\dagger-c_n)$ we get **Kitaev chain**:\n",
    "\n",
    "$$H=-\\mu\\sum_n c_n^\\dagger c_n-t\\sum_n (c_{n+1}^\\dagger c_n+\\textrm{h.c.}) + \\Delta\\sum_n (c_n c_{n+1}+\\textrm{h.c.})\\,.$$\n",
    "\n",
    "Three parameters:\n",
    "- onsite energy $\\mu$\n",
    "- hopping $t$ between different sites\n",
    "- the superconducting pairing $\\Delta$ (creates or annihilates pairs of particles)\n",
    "\n",
    "The unpaired Majorana regime is $\\Delta=t$ and $\\mu=0$, trivial regime is $\\Delta=t=0$ and $\\mu\\neq 0$.\n",
    "\n",
    "### Bogoliubov-de Gennes formalism\n",
    "\n",
    "Because the Hamiltonian is quadratic in fermions, we can use *Bogoliubov-de Gennes formalism* $H = \\tfrac{1}{2} C^\\dagger H_\\textrm{BdG} C$ with $C=(c_1, \\dots, c_N, c_1^\\dagger, \\dots, c^\\dagger_N)^T$.\n",
    "\n",
    "The matrix $H_\\textrm{BdG}$ is $2N\\times 2N$ and acts on electrons and holes.\n",
    "\n",
    "We'll use Pauli matrices $\\tau$ in particle and hole space, and $\\left|n\\right\\rangle = (0,\\dots,1,0,\\dots)^T$ the $n$-th site of the chain.\n",
    "\n",
    "(So e.g. $C^\\dagger\\,\\tau_z\\,\\left|n\\right\\rangle\\left\\langle n\\right|\\,C = 2c_n^\\dagger c_n-1$)\n",
    "\n",
    "The Bogoliubov-de Gennes Hamiltonian is then:\n",
    "\n",
    "$$H_{BdG}=-\\sum_n \\mu \\tau_z\\left|n\\right\\rangle\\left\\langle n\\right|-\\sum_n \\left[(t\\tau_z+i\\Delta\\tau_y)\\,\\left|n\\right\\rangle\\left\\langle n+1 \\right| + \\textrm{h.c.}\\right].$$\n",
    "\n",
    "It has particle-hole symmetry from replacing $c \\leftrightarrow c^\\dagger$: $\\mathcal{P}H_\\textrm{BdG}\\mathcal{P}^{-1}=-H_\\textrm{BdG}$ with $\\mathcal{P}=\\tau_x\\mathcal{K}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Topological protection of Majoranas\n",
    "\n",
    "Is the appearance of isolated Majorana modes accidental due to tuning parameters?\n",
    "\n",
    "### Let's check!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from ipywidgets import interact\n",
    "import kwant\n",
    "import numpy as np\n",
    "from matplotlib import pyplot\n",
    "import matplotlib\n",
    "\n",
    "pyplot.rcParams['figure.figsize'] = (12, 8)\n",
    "pyplot.rcParams['font.size'] = 16"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sx = np.array([[0, 1], [1, 0]])\n",
    "sy = np.array([[0, 1j], [-1j, 0]])\n",
    "sz = np.array([[1, 0], [0, -1]])\n",
    "s0 = np.array([[1, 0], [0, 1]])\n",
    "\n",
    "def kitaev_chain(L=10):\n",
    "    lat = kwant.lattice.chain()\n",
    "    syst = kwant.Builder()\n",
    "\n",
    "    def onsite(site, mu): \n",
    "        return - mu * sz\n",
    "    \n",
    "    for x in range(L):\n",
    "        syst[lat(x)] = onsite\n",
    "\n",
    "    def hop(site1, site2, t, delta):\n",
    "        return -t * sz - 1j * delta * sy\n",
    "    \n",
    "    syst[kwant.HoppingKind((1,), lat)] = hop\n",
    "\n",
    "    return syst.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "syst = kitaev_chain(L=20)\n",
    "mus = np.linspace(0, 4, 20)\n",
    "kwant.plotter.spectrum(syst, x=('mu', mus), params=dict(\n",
    "    t=1, delta=1\n",
    "));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zero eigenmodes only split when $\\mu \\simeq 2t$. \n",
    "\n",
    "### What happens to the eigenvectors?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@interact(mu=(0.5, 4.0))\n",
    "def lowest_eigenstate(mu=0.5):\n",
    "    L = 20\n",
    "    H = kitaev_chain(L=L).hamiltonian_submatrix(params=dict(\n",
    "        mu=mu, t=1, delta=1\n",
    "    ))\n",
    "    vals, vecs = np.linalg.eigh(H)\n",
    "    pyplot.plot(abs(vecs[:, L-1])**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\Rightarrow$ the wave function of the Majoranas stays zero in the middle of the wire.\n",
    "\n",
    "How can we understand this? \n",
    "\n",
    "Because of particle-hole symmetry the spectrum has to be symmetric around zero energy. We have two zero energy levels far away from each other and separated by a gapped medium. Moving these levels from zero energy individually violates particle-hole symmetry:\n",
    "\n",
    "![](figures/level_deformation_fig.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Band structure point of view\n",
    "\n",
    "Because we see that something interesting is happening in the bulk, let's look at the band structure.\n",
    "\n",
    "$$ H(k) \\equiv \\left\\langle k\\right| H_\\textrm{BdG} \\left| k \\right\\rangle = (-2t\\cos{k}-\\mu)\\,\\tau_z + 2\\Delta \\tau_y\\sin{k}.$$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lat = kwant.lattice.chain()\n",
    "infinite_kitaev_chain = kwant.Builder(kwant.TranslationalSymmetry((1,)))\n",
    "\n",
    "def onsite(site, mu):\n",
    "    return - mu * sz\n",
    "\n",
    "infinite_kitaev_chain[lat(0)] = onsite\n",
    "\n",
    "def hop(site1, site2, t, delta):\n",
    "    return -t * sz - 1j * delta * sy\n",
    "\n",
    "infinite_kitaev_chain[kwant.HoppingKind((1,), lat)] = hop\n",
    "\n",
    "infinite_kitaev_chain = infinite_kitaev_chain.finalized()\n",
    "\n",
    "@interact(mu=(-4., 4.))\n",
    "def band_structure(mu=0.0):\n",
    "    kwant.plotter.bands(infinite_kitaev_chain, params=dict(\n",
    "        mu=mu, t=1, delta=1\n",
    "    ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\Rightarrow$ the band gap closing at $k=0$ or $k=\\pi$ creates or destroys Majoranas."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions\n",
    "\n",
    "* A single Majorana is protected from all noise\n",
    "* Kitaev chain creates Majoranas\n",
    "* Majoranas stay protected until the band gap closes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lecture 2: Making Majoranas realistic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Small gap limit\n",
    "\n",
    "Superconducting gap $\\Delta$ is usually very small. Let's compare band structures:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, (ax1, ax2) = pyplot.subplots(1, 2, sharey=True)\n",
    "\n",
    "ax1.set_ylim((-1, 1))\n",
    "for mu, ax, title in zip((-2.4, -1.6), (ax1, ax2), ('trivial', 'topological')):\n",
    "    ax.set_title(title)\n",
    "    kwant.plotter.bands(infinite_kitaev_chain, ax=ax, momenta=151, params=dict(\n",
    "        mu=mu, t=1, delta=0.1\n",
    "    ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that the topological band structure looks like a \"Mexican hat\": this is useful and is going to help later.\n",
    "\n",
    "Let's also change $\\mu \\rightarrow \\mu -2t$ so that the transition happens at $\\mu=0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The need for spin\n",
    "\n",
    "The Kitaev chain only has spinless fermions, we need to bring spin back!\n",
    "\n",
    "Just add the spin as an extra degeneracy (multiplying the Hamiltonian by $\\sigma_0$) doesn't work:  \n",
    "we get 2 Majoranas instead of 1.\n",
    "\n",
    "A better idea is to add also a Zeeman field $E_Z$ that removes the spin degeneracy:\n",
    "\n",
    "$$H = (k^2/2m - \\mu - B \\sigma_z) \\tau_z + 2 \\Delta \\tau_y k.$$\n",
    "\n",
    "When the Zeeman energy $|B| > \\mu$ we have one Majorana fermion at the end of the chain.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kron = np.kron\n",
    "lat = kwant.lattice.chain(norbs=4)\n",
    "\n",
    "spinful_kitaev_chain = kwant.Builder(kwant.TranslationalSymmetry((1,)))\n",
    "\n",
    "def onsite(site, mu, B, t): \n",
    "    return (2*t - mu) * kron(s0, sz) + B * kron(sz, sz)\n",
    "\n",
    "spinful_kitaev_chain[lat(0)] = onsite\n",
    "\n",
    "def hop(site1, site2, t, delta):\n",
    "    return -t * kron(s0, sz) - 1j * delta * kron(s0, sy)\n",
    "\n",
    "spinful_kitaev_chain[kwant.HoppingKind((1,), lat)] = hop\n",
    "spinful_kitaev_chain = spinful_kitaev_chain.finalized()\n",
    "\n",
    "@interact(mu=(-1., 1.), B=(0, 1.))\n",
    "def band_structure(mu=0.0, B=0.0):\n",
    "    kwant.plotter.bands(spinful_kitaev_chain, momenta=151, params=dict(\n",
    "        mu=mu, t=1, delta=.1, B=B\n",
    "    ))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We resolved the first problem:\n",
    "\n",
    "> A high **Zeeman splitting** separates the different spins.\n",
    "> Then we make one spin species trivial, while the other one is topological and hosts Majoranas."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Realistic superconductivity\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The coupling in Kitaev chain is $p$-wave: $\\Delta \\propto k$, and it couples neighboring sites.\n",
    "\n",
    "Vast majority of the superconductors are $s$-wave $\\Rightarrow$ let's look at an $s$-wave superconductor + Zeeman field:\n",
    "\n",
    "$$\n",
    "H_\\textrm{BdG} = (k^2/2m - \\mu)\\tau_z + B \\sigma_z + \\Delta \\tau_x.\n",
    "$$\n",
    "\n",
    "([Note about basis change](https://topocondmat.org/w2_majorana/nanowire.html#Important-and-useful-basis-change.))\n",
    "\n",
    "At $k=0$ it has 4 levels with energies $E = \\pm B \\pm \\sqrt{\\mu^2 + \\Delta^2}$.\n",
    "\n",
    "At $B=0$ the system is trivial, therefore at\n",
    "\n",
    "$$ B^2 > \\Delta^2 + \\mu^2.$$\n",
    "\n",
    "it becomes nontrivial.\n",
    "\n",
    "Let's check if it works!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_wave_wire = kwant.Builder(kwant.TranslationalSymmetry((1,)))\n",
    "\n",
    "def onsite(site, mu, B, t, delta): \n",
    "    return (2*t - mu) * kron(s0, sz) + B * kron(sz, s0) + delta * kron(s0, sx)\n",
    "\n",
    "s_wave_wire[lat(0)] = onsite\n",
    "\n",
    "def hop(site1, site2, t):\n",
    "    return -t * kron(s0, sz)\n",
    "\n",
    "s_wave_wire[kwant.HoppingKind((1,), lat)] = hop\n",
    "s_wave_wire = s_wave_wire.finalized()\n",
    "\n",
    "@interact(mu=(-1., 1.), B=(0, 1.))\n",
    "def band_structure(mu=0.0, B=0.0):\n",
    "    kwant.plotter.bands(s_wave_wire, momenta=151, params=dict(\n",
    "        mu=mu, t=1, delta=.1, B=B\n",
    "    ))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The problem\n",
    "\n",
    "The spin-up and spin-down bands don't couple $\\Rightarrow$ no gap in the topological regime :("
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The final ingredient: spin-orbit coupling\n",
    "\n",
    "* No gap because of spin conservation $\\Rightarrow$ need to break the spin conservation.\n",
    "* Homogeneous magnetic field won't work.\n",
    "\n",
    "$\\Rightarrow$ we have to use [spin-orbit interaction](http://en.wikipedia.org/wiki/Rashba_effect):\n",
    "\n",
    "$$H_{SO} = \\alpha \\sigma_y k,$$\n",
    "\n",
    "(This term is invariant under time reversal symmetry: both $\\sigma_y$ and $k$ change sign).\n",
    "\n",
    "So now we have our final Hamiltonian:\n",
    "\n",
    "$$\n",
    "H_\\textrm{wire} = (k^2/2m + \\alpha \\sigma_y k - \\mu)\\tau_z + B \\sigma_z + \\Delta \\tau_x.\n",
    "$$\n",
    "\n",
    "Let's check it out once again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "majorana_wire = kwant.Builder(kwant.TranslationalSymmetry((1,)))\n",
    "\n",
    "def onsite(site, mu, B, t, delta): \n",
    "    return (2*t - mu) * kron(s0, sz) + B * kron(sz, s0) + delta * kron(s0, sx)\n",
    "\n",
    "majorana_wire[lat(0)] = onsite\n",
    "\n",
    "def hop(site1, site2, t, alpha):\n",
    "    return -t * kron(s0, sz) - 1j * alpha * kron(sy, sz)\n",
    "\n",
    "majorana_wire[kwant.HoppingKind((1,), lat)] = hop\n",
    "majorana_wire = majorana_wire.finalized()\n",
    "\n",
    "@interact(mu=(-1., 1.), B=(0, 1.), alpha=(0., 1.))\n",
    "def band_structure(mu=0.0, B=0.0, alpha=0.):\n",
    "    kwant.plotter.bands(majorana_wire, momenta=151, params=dict(\n",
    "        mu=mu, t=1, delta=.1, B=B, alpha=alpha\n",
    "    ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...and we created a topological superconductor!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions II\n",
    "\n",
    "Creating Majoranas in a real system requires:\n",
    "* A wire with 1 mode\n",
    "* $s$-wave superconductor\n",
    "* Zeeman field\n",
    "* Spin-orbit coupling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lecture III: Majorana signatures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Majorana conductance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Andreev reflection\n",
    "\n",
    "Charge transfer from a metal lead to a superconductor:\n",
    "\n",
    "![](figures/ns_interface.svg)\n",
    "\n",
    "When $eV < \\Delta$ electrons cannot enter the superconductor.\n",
    "\n",
    "They may undergo *normal reflection*:\n",
    "\n",
    "![](figures/normal_reflection.svg)\n",
    "\n",
    "(no current flows)\n",
    "\n",
    "Or they may undergo *Andreev reflection*:\n",
    "\n",
    "![](figures/andreev_reflection.svg)\n",
    "\n",
    "Andreev reflection transfers charge $2e$, and therefore results in conductance\n",
    "\n",
    "$$dI/dV \\equiv G(V)=2G_0|r_{eh}|^2.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Andreev conductance of Majoranas\n",
    "\n",
    "How do Majoranas change the picture:\n",
    "\n",
    "![](figures/ns_majorana_interface.svg)\n",
    "\n",
    "A useful analogy: Andreev *reflection* as *transmission* problem:\n",
    "\n",
    "![](figures/andreev_as_transmission.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Same with Majoranas:\n",
    "\n",
    "![](figures/resonant_transmission_through_majorana.svg)\n",
    "\n",
    "This system has:\n",
    "* A resonance at $E=0$\n",
    "* Symmetric double barrier\n",
    "\n",
    "Combined these give $r_{eh} = 1$ regardless of barrier strength.\n",
    "\n",
    "Let's check it out!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "majorana_wire = kwant.Builder(kwant.TranslationalSymmetry((-1,)))\n",
    "\n",
    "def onsite(site, mu, B, t, delta): \n",
    "    return (2*t - mu) * kron(s0, sz) + B * kron(sz, s0) + delta * kron(s0, sx)\n",
    "\n",
    "def hop(site1, site2, t, alpha):\n",
    "    return -t * kron(s0, sz) - 1j * alpha * kron(sy, sz)\n",
    "\n",
    "majorana_wire[lat(0)] = onsite\n",
    "majorana_wire[kwant.HoppingKind((1,), lat)] = hop\n",
    "\n",
    "\n",
    "tunnel_probe = kwant.Builder()\n",
    "\n",
    "def barrier(site, mu, mu_barrier, t): \n",
    "    return (2*t - mu - mu_barrier) * kron(s0, sz)\n",
    "\n",
    "tunnel_probe[lat(0)] = barrier\n",
    "\n",
    "\n",
    "lead = kwant.Builder(\n",
    "    kwant.TranslationalSymmetry((1,)),\n",
    "    conservation_law=kron(s0, sz)\n",
    ")\n",
    "\n",
    "def lead_onsite(site, mu_lead, t): \n",
    "    return (2*t - mu_lead) * kron(s0, sz)\n",
    "\n",
    "lead[lat(0)] = lead_onsite\n",
    "lead[kwant.HoppingKind((1,), lat)] = hop\n",
    "\n",
    "tunnel_probe.attach_lead(lead)\n",
    "tunnel_probe.attach_lead(majorana_wire)\n",
    "\n",
    "tunnel_probe = tunnel_probe.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delta = 0.05\n",
    "alpha = 0.08\n",
    "mu = .2\n",
    "voltages = np.linspace(-2*delta, 2*delta, 201)\n",
    "\n",
    "@interact(B=(0, .4, .01), mu_barrier=(0., 2.))\n",
    "def band_structure(B=0.0, mu_barrier=0.):\n",
    "    conductances = []\n",
    "    for V in voltages:\n",
    "        s = kwant.smatrix(tunnel_probe, energy=V, params=dict(\n",
    "            mu=mu, B=B, alpha=alpha, mu_lead=mu, delta=delta, t=1, mu_barrier=mu_barrier,\n",
    "        ))\n",
    "        conductances.append(2 - s.transmission((0, 0), (0, 0)) + s.transmission((0, 1), (0, 0)))\n",
    "    pyplot.plot(voltages, conductances)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that the quantized conductance in the topological regime does not disappear.\n",
    "\n",
    "Turns out this is due to the topology of the scattering matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Majorana supercurrent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What about coupling two Majoranas across a Josephson junction?\n",
    "\n",
    "Key idea: $H = i \\varepsilon(\\phi) \\gamma_1 \\gamma_2$\n",
    "\n",
    "How does $\\varepsilon$ depend on $\\phi$?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Superconducting flux quantum is $h/2e$ \n",
    "\n",
    "$\\Downarrow$\n",
    "\n",
    "$\\phi \\rightarrow \\phi + 2\\pi$ gives $\\gamma_2 \\rightarrow -\\gamma_2$.\n",
    "\n",
    "$\\Downarrow$\n",
    "\n",
    "$\\varepsilon(\\phi + 2\\pi) = -\\varepsilon(\\phi)$\n",
    "\n",
    "**The coupling between Majoranas is $4\\pi$-periodic.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see for ourselves!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "josephson_junction = kwant.Builder()\n",
    "\n",
    "def onsite(site, mu, mu_barrier, B, t, delta, phi):\n",
    "    normal = (2*t - mu) * kron(s0, sz) + B * kron(sz, s0)\n",
    "    if site.pos[0] > 0:\n",
    "        gap = delta * (np.cos(phi) * kron(s0, sx) + np.sin(phi) * kron(s0, sy))\n",
    "    else:\n",
    "        gap = delta * kron(s0, sx)\n",
    "    if site.pos[0] == 0:\n",
    "        normal += mu_barrier * kron(s0, sz)\n",
    "    return normal + gap\n",
    "\n",
    "def hop(site1, site2, t, alpha):\n",
    "    return -t * kron(s0, sz) - 1j * alpha * kron(sy, sz)\n",
    "\n",
    "for x in range(-40, 40):\n",
    "    josephson_junction[lat(x)] = onsite\n",
    "josephson_junction[kwant.HoppingKind((1,), lat)] = hop\n",
    "\n",
    "josephson_junction = josephson_junction.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@interact(B=(0.1, 0.5), mu_barrier=(0, 3))\n",
    "def plot_spectrum(B=0.4, mu_barrier=0.):\n",
    "    kwant.plotter.spectrum(\n",
    "        josephson_junction, x=('phi', np.linspace(0, 2*np.pi)),\n",
    "        params=dict(\n",
    "            mu=0.2, delta=0.2, B=B, alpha=.2, mu_barrier=mu_barrier, t=1\n",
    "        ), show=False)\n",
    "    pyplot.ylim(-0.3, 0.3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore:\n",
    "* Josephson junction couples Majoranas\n",
    "* This coupling has a $4\\pi$-periodicity, gives rise to $4\\pi$-periodic supercurrent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Braiding\n",
    "\n",
    "The most important signature of Majoranas and another reason why they are important for quantum computing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine slowly exchanging Majoranas:\n",
    "\n",
    "![](figures/nanowire_network_exchange.svg)\n",
    "\n",
    "The initial and final states are connected by a unitary operator $U$ ($U^{-1}=U^\\dagger$),\n",
    "\n",
    "$$\\left|\\Psi\\right\\rangle \\,\\to\\, U \\left|\\Psi\\right\\rangle\\,.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Hilbert space of a set of Majoranas\n",
    "\n",
    "Let's consider many Majoranas\n",
    "\n",
    "![](figures/gs_manifold.svg)\n",
    "\n",
    "To assign quantum states to Majoranas, we pair them into fermions:\n",
    "$$\n",
    "c^\\dagger_n = \\tfrac{1}{2}(\\gamma_{2n-1}+i\\gamma_{2n})\\,,\\\\\n",
    "c_n =\\tfrac{1}{2}(\\gamma_{2n-1}-i\\gamma_{2n})\\,,\n",
    "$$\n",
    "Every fermion is empty or occupied: $\\left|0\\right\\rangle$ and $\\left|1\\right\\rangle$.\n",
    "\n",
    "\n",
    "![](figures/majoranas_pairing.svg)\n",
    "\n",
    "Basis states are:\n",
    "\n",
    "$$\\left| s_1, s_2, \\dots, s_N\\right\\rangle\\,,$$\n",
    "\n",
    "All these are eigenstates of the parity $P_n \\equiv 1-2c^\\dagger_n c_n \\equiv i\\gamma_{2n-1}\\gamma_{2n}$. For instance, we have\n",
    "\n",
    "$$ P_1 \\left| 0, \\dots \\right\\rangle\\ = (1-2c^\\dagger_1 c_1)\\left|0, \\dots \\right\\rangle= + \\left|0, \\dots \\right\\rangle\\,, $$\n",
    "$$ P_1 \\left| 1, \\dots \\right\\rangle\\ = (1-2c^\\dagger_1 c_1)\\left|1, \\dots \\right\\rangle= - \\left|1, \\dots \\right\\rangle\\,, $$\n",
    "\n",
    "and so on. All $P_n$'s commute with each other, because they all involve a different pair of Majoranas. \n",
    " \n",
    "> Thus the  Hilbert space of states $|\\Psi\\rangle$ of a set of $N$ pairs of Majorana modes is spanned by the simultaneous eigenstates $|s_1,s_2,\\dots,s_N\\rangle$ of the commuting fermion parity operators $P_n$ and is written as $$\\left|\\Psi\\right\\rangle= \\sum_{s_n=0,1} \\alpha_{s_1s_2\\dots s_N}\\,\\left| s_1, s_2, \\dots, s_N\\right\\rangle\\, $$\n",
    " with complex coefficients $\\alpha_{s_1s_2\\dots s_N}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\left|\\Psi\\right\\rangle$ lies in the ground state manifold, which has $2^N$ states $\\Rightarrow$ $U$ is a $2^N\\times 2^N$ unitary matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deriving $U$\n",
    "\n",
    "1. Exchange of two Majoranas does not change the fermion parity $\\Rightarrow$ $[U, P_\\textrm{tot}]=0$.\n",
    "2. $U$ only depends on the Majoranas that are exchanged $\\Rightarrow$ $U = U(\\gamma_n,\\gamma_m)$\n",
    "\n",
    "Parity preservation $\\Rightarrow$ $U = U(\\gamma_n\\gamma_m)$.\n",
    "\n",
    "The only such unitary is:\n",
    "$$U\\equiv\\exp(\\beta \\gamma_n \\gamma_m) = \\cos(\\beta) + \\gamma_n\\gamma_m \\sin(\\beta)\\,,$$\n",
    "\n",
    "Let's use the [Heisenberg picture](http://en.wikipedia.org/wiki/Heisenberg_picture) and look at the evolution of $\\gamma_n$ and $\\gamma_m$:\n",
    "\n",
    "$$\n",
    "\\gamma_n\\,\\to\\,  U\\,\\gamma_n\\,U^\\dagger\\,,\\\\\n",
    "\\gamma_m\\,\\to\\,  U\\,\\gamma_m\\,U^\\dagger\\,.\n",
    "$$\n",
    "\n",
    "Inserting $U$ we obtain:\n",
    "\n",
    "$$\n",
    "\\gamma_n\\,\\to\\,   \\cos (2\\beta)\\,\\gamma_n  - \\sin(2\\beta)\\,\\gamma_m\\,,\\\\\n",
    "\\gamma_m\\,\\to\\,   \\cos (2\\beta)\\,\\gamma_m  + \\sin(2\\beta)\\,\\gamma_n\\,.\n",
    "$$\n",
    "\n",
    "We want $\\gamma_n \\leftrightarrow \\gamma_m$\n",
    "\n",
    "This only happens when $\\beta = \\pm \\pi/4$. ($\\pm$ distinguishes two directions of rotation.)\n",
    "\n",
    ">Thus, the unitary operator that exchanges the Majorana modes $\\gamma_n$ and $\\gamma_m$ is:\n",
    "$$U = \\exp \\left(\\pm\\frac{\\pi}{4}\\gamma_n \\gamma_m\\right) = \\tfrac{1}{\\sqrt{2}}\\left(1\\pm\\gamma_n\\gamma_m\\right)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Braiding 4 Majoranas\n",
    "\n",
    "\n",
    "Let's focus on Majoranas $\\gamma_1\\,\\gamma_2,\\gamma_3$ and $\\gamma_4$.\n",
    "\n",
    "Working out the exchanges of neighboring Majoranas $U_{12}, U_{23}$ and $U_{34}$ we get:\n",
    "\n",
    "$$\n",
    "U_{12} = \\exp\\left(\\frac{\\pi}{4}\\gamma_1 \\gamma_2\\right) \\equiv\\begin{pmatrix}\n",
    "e^{-i\\pi/4} & 0 & 0 & 0 \\\\0 & e^{i\\pi/4} & 0 &0 \\\\0 & 0& e^{-i\\pi/4} &0 \\\\ 0&0& 0& e^{i\\pi/4}\n",
    "\\end{pmatrix}\\,,\n",
    "$$\n",
    "\n",
    "$$\n",
    "U_{23} = \\exp\\left(\\frac{\\pi}{4}\\gamma_2 \\gamma_3\\right) \\equiv\\frac{1}{\\sqrt{2}}\\begin{pmatrix}\n",
    "1 & 0 & 0 & -i\\\\ 0 & 1 & -i& 0\\\\ 0& -i & 1 & 0\\\\ -i& 0& 0 & 1\n",
    "\\end{pmatrix}\\,,\n",
    "$$\n",
    "\n",
    "$$\n",
    "U_{34} = \\exp\\left(\\frac{\\pi}{4}\\gamma_3 \\gamma_4\\right) \\equiv\\begin{pmatrix}\n",
    "e^{-i\\pi/4} & 0 & 0 & 0\\\\ 0& e^{i\\pi/4} & 0& 0\\\\ 0& 0& e^{i\\pi/4} & 0\\\\ 0& 0& 0& e^{-i\\pi/4}\n",
    "\\end{pmatrix}\\,.\n",
    "$$\n",
    "\n",
    "For instance, if we start from the state $\\left|00\\right\\rangle$ and exchange $\\gamma_2$ and $\\gamma_3$, we obtain\n",
    "\n",
    "$$\\left|00\\right\\rangle\\,\\to\\,U_{23}\\left|00\\right\\rangle=\\tfrac{1}{\\sqrt{2}}\\left(\\left|00\\right\\rangle-i\\left|11\\right\\rangle\\right)\\,,$$\n",
    "\n",
    "which is a superposition of two states!\n",
    "\n",
    "Now it is not surprising that different braids do not commute:\n",
    "\n",
    "$$U_{23}U_{12}\\neq U_{12}U_{23}\\,$$\n",
    "\n",
    "> We have shown that exchanging two Majorana modes leads to a non-trivial rotation in the ground state manifold, and that changing the order of the exchanges matters. These properties make Majorana modes **non-Abelian anyons**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions III\n",
    "\n",
    "* Sending current through Majoranas gives a quantized conductance\n",
    "* Supercurrent through Majoranas has a periodicity of $4\\pi$ instead of $2\\pi$\n",
    "* Interchanging Majoranas transforms the many-body ground state"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
